# Authsome

Flexible attribute-based authorization module. This software is in a beta stage.

# Overview

Authsome is a minimal module with some syntax sugar surrounding a flexible attribute-based access control (ABAC) core. The external API consists of:

```
const authsome = new Authsome({mode: yourMode}, context)
const permission = authsome.can(user, operation, object)
```

The `authsome.can` bit is also named an authorization query/question in discussions and you'll see this term pop up in various places. They way your mode handles an authorization query is completely flexible, e.g. the simplest mode could look like: 

```
const yourMode = async function (user, operation, object, context) {
  return true
}
```

The above will just return true for any kind of authorization query, so it's not really useful (take a look at [example modes below](#example-modes)).

A useful but still minimal example might be something like this, where you instantiate a mode with a bit more `context`, in other words, more information about the environment where authorization questions are asked (e.g. how to find a user in the database):

```
const authsome = new Authsome({mode: yourMode}, {
  models: {
    User: {
      find: id => User.find(id)
    }
  }
})
```

This would proxy `models.User.find` to your `User.find`, which is accessible from where you instantiate Authsome, e.g. in [your server](https://gitlab.coko.foundation/pubsweet/pubsweet/blob/master/packages/server/src/helpers/authsome.js#L7) or [your client](https://gitlab.coko.foundation/pubsweet/pubsweet/blob/master/packages/client/src/helpers/withAuthsome.js#L13), and in practice this gives your mode access to your models or similarly important context. An authsome mode that knows about the User model can then do things like this:

```
module.exports = {
  before: async function (userId, operation, object, context) {
    const user = await context.models.User.find(userId)
    if (user.admin) {
      return true    
    }
  },
  someOperation: async function (userId, operation, object, context) {
    const user = await context.models.User.find(userId)
    if(user.allowedSomeOperation) {
      return true
    }
  }
```

The above gives you a good way to structure your authorization modes, where each property of your mode corresponds to handling of authorization queries for a certain operation. The one exception is the `before` property of a mode, which is evaluated before everything else, and is used to give e.g. admin users the permissions to do everything. The other property of the above example mode is `someOperation`, which corresponds to the `operation` parameter in a `authsome.can(user, operation, object)` query. 

However, storing if a user can or can not do `someOperation` on the User model is a bit like the RBAC (role-based authorization system), which often isn't flexible enough - this brings us to a topic that's out of scope for the Authsome module, but is absolutely in-scope for authorization modes that are implemented with Authsome. For example, authorization modes for PubSweet products are based around the concept of object-based Teams, which means that authorization is determined based on team membership. A team consists of a group of members and can be one of the configured team types. A team can be based around an object (the object of the authorization request), or can be a global team (not based around a specific object). An example of a team is, e.g. a team of type `reviewers`, which is based around a `manuscript` object, and when the question of `authsome.can(reviewerUser, 'review', manuscript)` is considered, the mode checks if the `status` property of the `manuscript` is 'in-review', and returns true if it is. For more examples of how teams are used to address authorization requirements, check out the advanced examples below.

# Example Modes

Modes are exchangeable mechanisms of authorization (e.g. built in example modes: [blog](https://gitlab.coko.foundation/pubsweet/authsome/blob/master/src/modes/blog/index.js), [journal](https://gitlab.coko.foundation/pubsweet/authsome/blob/master/src/modes/journal/index.js), [specific](https://gitlab.coko.foundation/pubsweet/authsome/blob/master/src/modes/specific/index.js), and [noon](https://gitlab.coko.foundation/pubsweet/authsome/blob/master/src/modes/noon/index.js)).

There are also real world/advanced examples (WIP) for [xpub (PubSweet-based journal publishing platform)](https://gitlab.coko.foundation/xpub/xpub/blob/e52a20537c9ab7447a1d316bb2decd26b14d4bb7/config/authsome.js) and [Editoria (PubSweet-based book production platform)](https://gitlab.coko.foundation/editoria/editoria/blob/editoria-authsome/packages/editoria-authsome/src/index.js).

# PubSweet starter a.k.a. Science Blogger example a.k.a. blog mode

In [PubSweet starter](https://gitlab.coko.foundation/pubsweet/pubsweet-starter), we have two types of teams. One is a 'contributor' type, which allows you to create blogposts for the blog. The other is a 'coauthor' team type, which allows you to update a specific blogpost (write it with someone). These are the only two team types.

When you're managing teams, you create a new team with a certain type, and a certain object (in the case of Contributors, you would choose the blog object as the object of the team). You can then add members to this team, and those members can then ask for and receive authorization to create blogposts for the blog.

Authsome can be used on both the server (for authorizing API requests, e.g. [for creating collections in PubSweet](https://gitlab.coko.foundation/pubsweet/pubsweet/blob/master/packages/server/src/routes/util.js#L115)), and client (for showing/hiding UI that relates to an authorization request, e.g. a [Create Blogpost button.](https://gitlab.coko.foundation/pubsweet/pubsweet/blob/master/packages/components/PostsManager/Post.jsx#L111))

