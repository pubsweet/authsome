const expect = require('expect.js')
const Authsome = require('../../../src')
const test = require('../test')

const pickBy = require('lodash/pickBy')

let user1 = {
  id: 'user1',
  name: 'Just A User',
  type: 'user'
}

let user2 = {
  id: 'user_2',
  name: 'User Two',
  type: 'user'
}

let filterUser = {
  id: 'user_3',
  name: 'User Three',
  type: 'user'
}

let collection = {
  id: 'collection_1',
  name: 'Blog',
  owners: [user1.id],
  type: 'collection'
}

let fragment = {
  id: 'fragment_1',
  title: 'Post',
  owners: [user2.id],
  parents: [collection],
  type: 'fragment'
}

let authsome = new Authsome({mode: test})

describe('test mode', function () {
  describe('a user we filter', () => {
    it('should return an object filter when asking about reading all collections', () => {
      return authsome.can(filterUser, 'GET', 'collections').then(result => {
        expect(result.permission).to.eql('filter')
        expect(result.filter).to.be.a('function')

        let filtered = [collection].filter(result.filter)
        expect(filtered).to.eql([])
      })
    })

    it('should return a property filter when asking about reading a collection', () => {
      return authsome.can(filterUser, 'GET', collection).then(result => {
        expect(result.permission).to.eql('filter')
        expect(result.filter).to.be.a('function')

        let filtered = pickBy(collection, result.filter)

        expect(filtered).to.eql({id: 'collection_1'})
      })
    })
  })

  describe('just a random user', function () {
    it('should not be able to delete fragments owned by someone else', function () {
      return authsome.can(user1, 'DELETE', fragment).then(permission => {
        expect(permission).to.eql(false)
      })
    })

    it('should be able to create fragments', function () {
      return authsome.can(user2, 'POST', fragment).then(permission => {
        expect(permission).to.eql(true)
      })
    })
  })

  describe('unauthenticated user', function () {
    it('should not be able to create a fragment', function () {
      return authsome.can(undefined, 'POST', fragment).then(permission => {
        expect(permission).to.eql(false)
      })
    })

    it('should be able to read a public fragment', function () {
      let publicFragment = Object.assign({public: true}, fragment)
      return authsome.can(undefined, 'GET', publicFragment).then(permission => {
        expect(permission).to.eql(true)
      })
    })
  })

  describe('contextualized request', function () {
    it('should be able to delete a fragment if owner of parent collection', function () {
      const context = {
        models: {
          collection: {
            find: (id) => Promise.resolve(collection)
          }
        }
      }
      authsome = new Authsome({mode: test}, context)
      return authsome.can(user1, 'DELETE', fragment).then(permission => {
        expect(permission).to.eql(true)
      })
    })
  })
})
