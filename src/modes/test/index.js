// WARNING: Used purely for testing reasons
// - Everything is allowed for owners of objects or the objects' parents
// - Nothing is allowed for unauthenticated users
// - Public things can be read
// - User with id user_3 gets filteret results
var test = function (user, operation, object, context) {
  if (!user) {
    if (operation === 'GET' && object.public) {
      return true
    }
    return false
  }

  // Example of a filtered authorization response
  // In this case:
  // 1. a filter that returns user_3's collections
  // 2. a filter that returns the 'id' property
  if (user && user.id === 'user_3') {
    if (object === 'collections') {
      return {
        permission: 'filter',
        filter: (object) => object.owners.includes('user_3')
      }
    } else if (object && object.type === 'collection') {
      return {
        permission: 'filter',
        filter: (value, key) => key === 'id'
      }
    }
  }

  if (user && operation && object.owners.includes(user.id)) {
    return true
  }

  if (user && operation && object && context) {
    if (object.public) {
      return true
    }

    return context.models.collection.find(object.parents[0]).then(collection => {
      if (collection.owners.includes(user.id)) {
        return true
      }
    })
  }

  return false
}

module.exports = test
