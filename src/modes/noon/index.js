var noon = function (user, operation, object) {
  let fragment = object
  let collection = fragment.parents[0]

  if (user.admin === true) return true

  if (operation === 'update' || operation === 'delete') {
    for (const fragmentOwnerId of fragment.owners) {
      if (fragmentOwnerId === user.id) {
        return true
      }
    }
  }

  if (operation === 'create') {
    for (const collectionOwnerId of collection.owners) {
      if (collectionOwnerId === user.id) {
        return true
      }
    }
  }

  for (const team of user.teams) {
    if (team.teamType.active()) {
      if (team.teamType.permissions === 'all' &&
          team.object === collection &&
          operation === 'create') {
        return true
      }

      if (team.teamType.permissions === 'all' &&
        team.object === fragment &&
        (operation === 'update' || operation === 'delete')) {
        return true
      }

      if (team.teamType.permissions === 'create' &&
          team.object === collection &&
          operation === 'create') {
        return true
      }

      if (team.teamType.permissions === 'update' &&
          team.object === fragment &&
          operation === 'update') {
        return true
      }
    }
  }
  return false
}

module.exports = noon
