function isAllowed (operation, permission) {
  if ((operation === 'read' || operation === 'update' || operation === 'delete') &&
    permission === 'delete') {
    return true
  }

  if ((operation === 'read' || operation === 'update') &&
    permission === 'update') {
    return true
  }

  if ((operation === 'read') &&
    permission === 'read') {
    return true
  }

  if ((operation === 'create') &&
    permission === 'create') {
    return true
  }
}

function can (user, operation, object) {
  // Publicly-readable
  if (object.type === 'fragment' &&
      object.state === 'published' &&
      operation === 'read') {
    return true
  } else if (user === undefined) {
    return false
  }

  // Admins and Owners (also owners of parent objects)

  if (user.admin === true) return true

  for (const objectOwnerId of object.owners) {
    if (objectOwnerId === user.id) {
      return true
    }
  }

  if (Array.isArray(object.parents)) {
    for (const parent of object.parents) {
      if (parent.owners.includes(user)) {
        return true
      }
    }
  }

  // Teams

  for (const team of user.teams) {
    if (team.teamType.active) {
      if (isAllowed(operation, team.teamType.permissions) &&
          team.object === object &&
          team.teamType.active(object)) {
        return true
      }
    } else {
      if (isAllowed(operation, team.teamType.permissions) &&
          team.object === object) {
        return true
      }
    }
  }

  // Users with permissions for parents of object
  // This is only applicable in certain states

  if ((object.state === 'submitted' || object.state === undefined) &&
    Array.isArray(object.parents)) {
    for (const parent of object.parents) {
      if (can(user, operation, parent)) {
        return true
      }
    }
  }

  return false
}

module.exports = can
