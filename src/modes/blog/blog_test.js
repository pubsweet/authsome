const Authsome = require('../../../src')
const blog = require('../blog')

// CREATE UPDATE DELETE

const admin = {
  id: 'user_1',
  name: 'Admin user',
  admin: true,
  type: 'user'
}

const collection = {
  id: 'collection_1',
  title: 'Blog',
  owners: [admin.id],
  type: 'collection'
}

const teams = {
  teamContributors: {
    name: 'Contributors',
    permissions: 'POST'
  },
  teamCoauthors: {
    name: 'Coauthors',
    permissions: 'PATCH'
  }
}

const teamContributors = {
  id: 'team_1',
  object: collection,
  type: 'team',
  teamType: teams.teamContributors
}

const user2 = {
  id: 'user_2',
  name: 'User One',
  teams: [teamContributors],
  type: 'user'
}

const fragment = {
  id: 'fragment_1',
  title: 'Post',
  owners: [user2.id],
  parents: [collection],
  type: 'fragment'
}

const teamCoauthors = {
  id: 'team_2',
  object: fragment,
  type: 'team',
  teamType: teams.teamCoauthors
}

const user3 = {
  id: 'user_3',
  name: 'User Two',
  teams: [teamCoauthors],
  type: 'user'
}

const users = [admin, user2, user3]
const authsome = new Authsome({mode: blog, teams}, {
  models: {
    User: {
      find: id => users.find(user => user.id === id)
    }
  }
})

describe('Blog mode', function () {
  describe('admin', function () {
    it('should be able to delete fragments owned by someone else', async () => {
      const permission = await authsome.can(admin.id, 'DELETE', fragment)
      expect(permission).toBe(true)
    })

    it('should be able to create fragments', async () => {
      const permission = await authsome.can(admin.id, 'POST', fragment)
      expect(permission).toBe(true)
    })
  })

  describe('member of team contributors', function () {
    it('should be able to access own collection', async () => {
      const permission = await authsome.can(user2.id, 'GET', {
        ...collection,
        owners: [user2.id]
      })
      expect(permission).toBe(true)
    })

    it('should be able to access published fragment from other user', async () => {
      const permission = await authsome.can(user2.id, 'GET', {
        ...fragment,
        owners: [user3.id],
        published: true
      })
      expect(permission).toBeTruthy()
    })

    it('should be able to create fragments', async () => {
      const permission = await authsome.can(user2.id, 'POST', fragment)
      expect(permission).toBe(true)
    })

    it('should not be able to delete fragments which are owned by someone else', async () => {
      const permission = await authsome.can(user2.id, 'POST', fragment)
      expect(permission).toBe(true)
    })

    it('should be able to update fragments owned by them', async () => {
      const permission = await authsome.can(user2.id, 'PATCH', fragment)
      expect(permission).toBe(true)
    })

    it('should not be able to update fragments owned by other users', async () => {
      const permission = await authsome.can(user2.id, 'PATCH', {...fragment, owners: [user3.id]})
      expect(permission).toBe(false)
    })

    it('should not be able to access other user', async () => {
      const permission = await authsome.can(user2.id, 'GET', user3)
      expect(permission).toBe(false)
    })
  })

  describe('member of team coauthors', () => {
    it('should be able to update the fragment', async () => {
      const permission = await authsome.can(user3.id, 'PATCH', fragment)
      expect(permission).toBe(true)
    })
  })

  describe('deleted user', () => {
    it('should be able to get collection', async () => {
      const permission = await authsome.can('bad id', 'GET', collection)
      expect(permission).toBeTruthy()
    })

    it('should not be able to update collection', async () => {
      const permission = await authsome.can('bad id', 'PATCH', collection)
      expect(permission).toBe(false)
    })
  })

  describe('member of public', () => {
    it('should be able to access collections', async () => {
      const permission = await authsome.can(null, 'GET', {path: '/collections'})
      expect(permission).toBeTruthy()
    })

    it('should be able to access published fragments', async () => {
      const permission = await authsome.can(null, 'GET', {...fragment, published: true})
      expect(permission).toBeTruthy()
    })

    it('should not be able to access unpublished fragments', async () => {
      const permission = await authsome.can(null, 'GET', fragment)
      expect(permission).toBe(false)
    })

    it('should filter fragments properties', async () => {
      const publishedFragment = {...fragment, published: true, foobar: 'nope'}
      const permission = await authsome.can(null, 'GET', publishedFragment)
      expect(permission.filter).toBeTruthy()
      expect(permission.filter(publishedFragment)).toEqual({
        id: 'fragment_1',
        type: 'fragment',
        title: 'Post',
        owners: [user2.id]
      })
    })

    it('should filter collection properties', async () => {
      const unfilteredCollection = {...collection, secret: 'the cake is a lie'}
      const permission = await authsome.can(null, 'GET', unfilteredCollection)
      expect(permission.filter).toBeTruthy()
      expect(permission.filter(unfilteredCollection)).toEqual({
        id: 'collection_1',
        type: 'collection',
        title: 'Blog',
        owners: [admin.id]
      })
    })

    it('should filter unpublished fragments', async () => {
      const permission = await authsome.can(null, 'GET', {path: '/collections/:id/fragments'})
      expect(permission.filter).toBeTruthy()
      expect(permission.filter([{id: 1}, {id: 2, published: true}])).toEqual([{id: 2, published: true}])
    })
  })
})
