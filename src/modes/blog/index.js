const pick = require('lodash/pick')

function isPublished (fragment) {
  return fragment.published
}

function isOwner (user, object) {
  if (!object || !object.owners || !user) {
    return false
  }

  return object.owners.includes(user.id)
}

function teamPermissions (user, operation, object) {
  if (!user || !Array.isArray(user.teams)) {
    return false
  }

  return user.teams.some(team => {
    return ['POST', 'PATCH'].includes(operation) &&
      operation === team.teamType.permissions &&
      team.object.id === object.id
  })
}

function unauthenticatedUser (operation, object) {
  // Public/unauthenticated users can GET /collections
  if (operation === 'GET' && object && object.path === '/collections') {
    return true
  }

  // Public/unauthenticated users can GET /collections/:id/fragments, filtered by 'published'
  if (operation === 'GET' && object && object.path === '/collections/:id/fragments') {
    return {
      filter: fragments => fragments.filter(isPublished)
    }
  }

  // and filtered individual collection's properties: id, title, source, content, owners
  if (operation === 'GET' && object && object.type === 'collection') {
    return {
      filter: collection => pick(collection, ['id', 'type', 'title', 'owners', 'fragments'])
    }
  }

  if (operation === 'GET' && object && object.type === 'fragment' && object.published) {
    return {
      filter: fragment => pick(fragment, ['id', 'type', 'title', 'source', 'presentation', 'owners'])
    }
  }

  return false
}

async function blog (userId, operation, object, context) {
  if (!userId) {
    return unauthenticatedUser(operation, object)
  }

  const user = await context.models.User.find(userId)

  // Admins can do anything
  if (user && user.admin === true) return true

  if (object) {
    switch (object.type) {
      case 'collection':
        const collection = object
        if (isOwner(user, collection)) {
          return true
        }
        if (teamPermissions(user, operation, collection)) {
          return true
        }
        break

      case 'fragment':
        const fragment = object

        if (isOwner(user, fragment)) {
          return true
        }

        if (teamPermissions(user, operation, fragment)) {
          return true
        }

        if (Array.isArray(fragment.parents) && teamPermissions(user, operation, fragment.parents[0])) {
          return true
        }
        break

      case 'user':
        if (user.id === object.id) {
          return true
        }
    }
  }

  // ensure a logged in user can do anything a non-logged in user can
  return unauthenticatedUser(operation, object)
}

module.exports = blog
