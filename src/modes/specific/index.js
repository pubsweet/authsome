module.exports = {
  'submit review': async function (userId, operation, object, context) {
    const user = await context.models.User.find(userId)
    return user.name === 'User One' && object.title === 'Book by User One'
  },
  // This gets called before all other queries
  before: async function (userId, operation, object, context) {
    const user = await context.models.User.find(userId)
    return user.admin
  }
}
