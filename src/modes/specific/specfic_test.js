const Authsome = require('../../../src')
const specific = require('../specific')

const user = {
  id: 'user_1',
  name: 'User One'
}

const admin = {
  id: 'user_2',
  name: 'Admin User',
  admin: true
}

const someThing = {
  title: 'Book by User One'
}

const authsome = new Authsome({mode: specific}, {
  models: {
    User: {
      find: id => Promise.resolve([user, admin].find(user => user.id === id))
    }
  }
})

describe('Specific mode', function () {
  describe('specific query', function () {
    it('can respond to specific operations/queries', async () => {
      const permission = await authsome.can(user.id, 'submit review', someThing)
      expect(permission).toBe(true)
    })

    it('defaults to false for undefined operations/queries', async () => {
      const permission = await authsome.can(user.id, 'submit paper', someThing)
      expect(permission).toBe(false)
    })

    it('supports a fallback for undefined operations/queries', async () => {
      const permission = await authsome.can(admin.id, 'do an unknown thing', someThing)
      expect(permission).toBe(true)
    })
  })
})
