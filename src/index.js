'use strict'

// Config is used to provide mode name and team configuration, e.g.:
//
//   const blogmode = require('authsome/src/modes/blog')
//   authsome: {
//     mode: blogmode,
//     teams: {
//       teamContributors: {
//         name: 'Contributors',
//         permissions: 'create'
//       },
//       teamCoauthors: {
//         name: 'Coauthors',
//         permissions: 'update'
//       }
//     }
//   }
//
// Context can be used to supply additional information about
// the authorization request. For example, you can use it to supply
// information about the model system to the authorization system, e.g.:
//
//   let authsome = new Authsome(config.authsome, {
//     models: require('../models')
//   })

class Authsome {
  constructor (config, context) {
    if (config) {
      this.mode = config.mode
      this.teams = config.teams
    }

    this.context = context

    if (!this.mode) {
      this.mode = 'freefornone'
      this.teams = undefined
    }
  }
  static promisify (result) {
    let isPromise = typeof result.then === 'function'
    return isPromise ? result : Promise.resolve(result)
  }

  can (user, operation, object) {
    if (this.mode === 'freefornone') {
      return Promise.resolve(false)
    } else {
      // Backwards compatiblity
      const modeType = typeof this.mode
      let mode
      let before
      if (modeType === 'function') {
        mode = this.mode
      } else if (modeType === 'object') {
        before = this.mode.before
        mode = this.mode[operation]
      }

      // Try to use the before handler before specific ones
      if (before) {
        return before(user, operation, object, this.context).then(beforeResult => {
          if (beforeResult) {
            return beforeResult
          } else if (mode) {
            let result = mode(user, operation, object, this.context)
            return Authsome.promisify(result)
          } else {
            return false
          }
        })
      }

      // Use the mode to determine permissions
      if (mode) {
        let result = mode(user, operation, object, this.context)
        return Authsome.promisify(result)
      } else {
        // If mode or operation aren't implemented, default to false
        return Promise.resolve(false)
      }
    }
  }
}

module.exports = Authsome
