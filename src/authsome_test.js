const Authsome = require('.')

describe('Authsome', () => {
  it('defaults to freefornone for no mode', () => {
    const auth = new Authsome()
    expect(auth.mode).toBe('freefornone')
  })

  it('accepts mode as function', () => {
    const myMode = () => false
    const auth = new Authsome({mode: myMode})
    expect(auth.mode).toBe(myMode)
  })

  describe('freefornone mode', () => {
    it('always returns false', async () => {
      const auth = new Authsome()
      expect(await auth.can()).toBe(false)
    })
  })
})
